import React, { useState } from 'react'
import { Icon, Button } from 'semantic-ui-react'
import uuid from 'uuid';
import { Picker, Emoji } from 'emoji-mart'
import IMessage from '../../interfaces/IMessage';
import IUser from '../../interfaces/IUser';

import styles from './styles.module.css';

interface InputBlockProps {
  user: IUser,
  sendMessage(message: IMessage): void,
  setEditMsg(message: IMessage): void,
  LastMessage: IMessage | null
}

const InputBlock: React.FC<InputBlockProps> = ({ user, sendMessage, setEditMsg, LastMessage }) => {
  const [text, setText] = useState('');
  const [picker, setPicker] = useState(false);

  const toglePicker = () => {
    setPicker(!picker);
  }

  const changeHandler = async (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    setText(event.target.value);
  }


  const keyPressHandler = (event: React.KeyboardEvent) => {
    if (event.key === 'Enter' && text.trim() !== '') {
      handleSend();
    }
  }

  const handleSend = () => {
    const messege = {
      id: uuid.v4(),
      text,
      user: user.name,
      avatar: user.avatar,
      userId: user.id,
      editedAt: '',
      createdAt: new Date().toUTCString(),
      isLiked: false
    }
    sendMessage(messege);
    setPicker(false);
    setText('');
  };

  const addEmoji = (emogi: any) => {
    setText(`${text}${emogi.native}`);
  }

  return (
    <div className={styles.container}>
      {picker &&
        <div className={styles.picker}>
          <Picker native onSelect={addEmoji} showSkinTones={false} emojiSize={16} showPreview={false} />
        </div>
      }
      <div className={`${styles.inputBlock} segment`}>
        <div className={styles.text}>
          <textarea
            rows={5}
            placeholder='Tell us more'
            value={text}
            onChange={changeHandler}
            onKeyPress={keyPressHandler}
          />
          <div className={styles.smileBtn} onClick={toglePicker}>
            <Emoji emoji=':smiley:' size={16} />
          </div>
        </div>
        <div className={styles.btnBlock}>
          {LastMessage &&
            <Button common className={styles.btn} floated='right' onClick={() => setEditMsg(LastMessage)}>
              <Icon name='arrow up' />
            </Button>}
          <Button primary className={styles.btn} disabled={text.trim() === ''} floated='right' onClick={handleSend}>Send</Button>
        </div>
      </div>
    </div>
  )
}

export default InputBlock;