
import React from 'react';
import InputBlock from '../../components/InputBlock';
import ChatHeader from '../../components/Header';
import MessageBox from '../../components/MessageBox';
import { setUser } from '../Profile/actions';
import * as timeConverter from '../../helpers/TimeConverter';
import * as msgActions from './actions'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import MsgEditModale from '../../components/MsgEditModale';
import styles from './styles.module.css';
import IMessage from '../../interfaces/IMessage';
import IUser from '../../interfaces/IUser';

interface ChatProps {
  messages: IMessage[],
  user: IUser,
  editedMsg: IMessage | undefined,
  isLoading: boolean | undefined,
  setUser(): void,
  setIsLoading(isLoading: boolean): void,
  loadMessages(): void,
  addMessage(message: IMessage): void,
  updateMessage(message: IMessage): void,
  deleteMessage(MsgId: string): void,
  likeMessage(messageId: string): void,
  setEditedMessege(message: IMessage | undefined): void
}

class Chat extends React.Component<ChatProps> {
  constructor(props: ChatProps) {
    super(props);
    this.getParticipantCount = this.getParticipantCount.bind(this);
    this.addMessage = this.addMessage.bind(this);
    this.updateMessage = this.updateMessage.bind(this);
    this.deleteMessage = this.deleteMessage.bind(this);
    this.likeMessage = this.likeMessage.bind(this);
    this.getLastMessageDate = this.getLastMessageDate.bind(this);
    this.getLastMessage = this.getLastMessage.bind(this);
  }

  async componentDidMount() {
    this.props.setUser();
    this.props.setIsLoading(true)
    this.props.loadMessages();
  }

  addMessage(newMessage: IMessage) {
    this.props.addMessage(newMessage);
  }

  updateMessage(updatedMessage: IMessage) {
    this.props.updateMessage(updatedMessage);
  }

  deleteMessage(MsgId: string) {
    this.props.deleteMessage(MsgId);
  }

  likeMessage(messageId: string) {
    this.props.likeMessage(messageId);
  }

  getParticipantCount() {
    var unicUsers = new Set<string>();
    this.props.messages.map(msg => unicUsers.add(msg.user));
    return unicUsers.size;
  }

  getLastMessageDate() {
    const messages = this.props.messages;
    if (messages.length === 0) {
      return 'never';
    }
    const lastMsg = messages[messages.length - 1];
    return timeConverter.getDividerFormatDate(lastMsg.createdAt);
  }

  getLastMessage(): IMessage | null {
    const messages = this.props.messages;
    if (messages.length === 0) {
      return null;
    }
    const lastMsg = messages[messages.length - 1];
    return lastMsg.userId === this.props.user.id ? lastMsg : null;
  }

  render() {
    return (
      <div className={styles.application}>
        <ChatHeader
          participants={this.getParticipantCount()}
          messages={this.props.messages!.length}
          mostRecentDate={this.getLastMessageDate()}
        />
        <MessageBox
          isLoading={this.props.isLoading}
          messages={this.props.messages}
          getDate={timeConverter.getDateWithoutTime}
          deleteMessage={this.deleteMessage}
          user={this.props.user}
          likeMessage={this.likeMessage}
          setEditMsg={this.props.setEditedMessege}
        />
        <InputBlock
          user={this.props.user}
          sendMessage={this.addMessage}
          LastMessage={this.getLastMessage()}
          setEditMsg={this.props.setEditedMessege}
        />
        {this.props.editedMsg &&
          <MsgEditModale
            editFunc={this.updateMessage}
            message={this.props.editedMsg}
            close={() => this.props.setEditedMessege(undefined)}
          />}
      </div>
    );
  }
}


const mapStateToProps = (rootState: any) => ({
  messages: rootState.messages.messages,
  user: rootState.profile.user,
  editedMsg: rootState.messages.editedMsg,
  isLoading: rootState.messages.isLoading
});

const actions = {
  ...msgActions,
  setUser
};

const mapDispatchToProps = (dispatch: any) => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Chat);
