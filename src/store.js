import {
  createStore,
  applyMiddleware,
  combineReducers
} from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import chatReducer from './container/Chat/reduser';
import profileReducer from './container/Profile/reduser';

const initialState = {
  messages: {messages: []},
  profile: {user: {id: "id", name: "name", avatar: ""}}
};

const reducers = {
  messages: chatReducer,
  profile: profileReducer
};

const rootReducer = combineReducers({ ...reducers });

const store = createStore(
  rootReducer,
  initialState,
  composeWithDevTools(
    applyMiddleware(thunk),
  )
);

export default store;
